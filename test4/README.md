# 实验4：PL/SQL语言打印杨辉三角
 姓名：贾丫丫          学号：202010414406         班级：4班

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

## 杨辉三角源代码

```
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```



创建存储过程：

```
create or replace PROCEDURE YHTRLANGLE(p_rows IN INTEGER) AS
type t_number is varray (100) of integer not null;
i integer;
j integer;
spaces varchar2(30) :='   '; 
N integer := p_rows; 
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); 
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put_line(''); 
   
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N 
    loop
        rowArray(i):=1;    
        j:=i-1;
           
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));
        end loop;
        dbms_output.put_line(''); 
    end loop;
END;
```



在hr用户进行存储过程的调用：

```
set SERVEROUTPUT on
begin
YHTRLANGLE(5);
end;·
```
# 实验总结：

通过源代码实现了杨辉三角的打印。在代码中，使用了PL/SQL语言的变量、循环和数组等特性。首先打印了杨辉三角的第一行和第二行，然后使用循环逐行打印后续的数字。创建一个存储过程。存储过程是一段预先编译并存储在数据库中的可重复使用的代码块。在存储过程中，使用了与源代码相同的逻辑来打印杨辉三角，但在循环次数上改为使用传入的参数。在hr用户中调用存储过程。通过使用PL/SQL块，使用begin和end关键字将存储过程的调用代码包裹起来。
通过本次实验，我们了解了在Oracle数据库中使用PL/SQL语言编写代码、创建存储过程和调用存储过程的基本过程。杨辉三角的例子展示了PL/SQL语言的循环、数组和输出功能的应用。存储过程的创建和调用可以提高代码的复用性和可维护性，方便在需要的时候重复使用相同的逻辑。