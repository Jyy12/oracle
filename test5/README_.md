# 实验5：包，过程，函数的用法
 姓名：贾丫丫          学号：202010414406         班级：4班

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```



## 脚本代码

```sql
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```


## 测试

```sh

函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;


过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

```



## 实验总结
创建了一个名为MyPack的包，包含了一个函数和一个过程。函数Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)：该函数用于计算指定部门ID的员工薪资总和。通过查询EMPLOYEES表中部门ID为指定值的员工记录，并使用SUM函数计算薪资总和，最后将结果返回。过程Get_Employees(V_EMPLOYEE_ID NUMBER)：该过程用于展示从指定员工ID开始的员工层级关系。通过递归查询的方式，从EMPLOYEES表中以指定员工ID为起点，逐级查询其下属员工，并使用DBMS_OUTPUT.PUT_LINE函数输出员工的ID和姓名。
最后进行测试：对函数Get_SalaryAmount进行了测试，查询了每个部门的部门ID、部门名称和员工薪资总和，并将结果进行输出展示。对过程Get_Employees进行了测试，指定了一个员工ID，通过递归查询展示了从该员工开始的员工层级关系，并将结果通过DBMS_OUTPUT.PUT_LINE函数进行输出展示。
通过本次实验，我们深入了解了Oracle数据库中包的概念和使用方法。包可以将相关的功能封装在一起，提高代码的可维护性和重用性。同时，学习了递归查询的语法和应用，递归查询在处理具有层级关系的数据时具有重要作用，能够方便地遍历和处理层级数据。通过实验，加深了对包和递归查询的理解和应用能力。
